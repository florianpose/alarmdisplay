vim: tw=78 spl=en spell


[[__TOC__]]

# Alarm Display

This is an alarm display implementation for fire departments (etc). It has
been designed for the local circumstances of the Kreis Kleve, North
Rhine-Westphalia, Germany. It should be adaptable for other areas and
countries.

![Screenshot of alarm mode](images/screenshot-alarm.png)

![Screenshot of idle mode](images/screenshot-idle.png)

## Hardware

The original hardware is a Raspberry Pi with a large display connected via
HDMI and a digital alarm pager connected via USB.

## Software

The software is written in Python3, using the PyQt libaries for Qt5.

## Features

- Alarm data display with target map and route map
- Maps using OpenStreetMap tiles or custom tiles with fire hydrants
- Alarm report document for printer output
- Receive alarms from pagers, IMAP or WebSockets (JSON)
- Show previous alarms, calendar information, weather data in idle mode
- Switch screen on and off via CEC
- Forward alarms to subsequent displays
- Use GPIO pins to switch lighting or open doors
- Play sounds and read alarm data with text-to-speech engines
- Display vehicle status

## Installation

When using Raspberry PI OS, the necessary packages can be installed with the
following command:

```bash
apt-get install \
    cec-utils \
    cups \
    cups-bsd \
    imagemagick \
    git \
    libsox-fmt-mp3 \
    pandoc \
    pulseaudio-utils \
    python-libcec \
    python3-babel \
    python3-caldav \
    python3-cheetah \
    python3-icalendar \
    python3-lxml \
    python3-mpltoolkits.basemap \
    python3-numpy \
    python3-openssl \
    python3-pip \
    python3-pyproj \
    python3-pyqt5 \
    python3-pyqt5.qtsvg \
    python3-requests \
    python3-rpi.gpio \
    python3-serial \
    python3-tzlocal \
    python3-urllib3 \
    python3-websocket \
    sox \
    texlive-fonts-recommended \
    texlive-latex-base \
    texlive-latex-recommended \
    x11-xserver-utils \
    x11vnc \
    xinit
```

These additional Python modules are needed which are not packaged in Raspberry
Pi OS:

```bash
pip3 install \
    astral \
    gtts \
    imapclient \
    websocket-client
```

These additional steps are necessary after installation.

- Copy `alarmdisplay-sample.conf` to `/etc/alarmdisplay.conf` and adapt it to
  your needs.
- Copy a set of OpenStreetMap tiles to your filesystem and point the
  alarmdisplay software and set the variable `tiles_dir` (section `[maps]`)
- Optional: Copy a logo to the filesystem and set the `logo` variable in
  section `[general]`.
