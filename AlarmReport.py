# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Alarm Report
#
# Copyright (C) 2018 Florian Pose
#
# This file is part of Alarm Display.
#
# Alarm Display is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alarm Display is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alarm Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import os
import codecs
import tempfile
from Cheetah.Template import Template
import subprocess
import shutil
import re

import email.utils
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
import smtplib

import Map
from LaTeX import escapeLaTeX


# ----------------------------------------------------------------------------

class AlarmReport:

    def __init__(self, config, logger):
        self.config = config
        self.logger = logger

        res = 200 / 25.4  # [px / mm]
        self.map_width = res * 120
        self.map_height = res * 120

        # Load Template
        templateDir = self.config.get("report", "template_dir",
                                      fallback="report")
        templatePath = os.path.join(templateDir, 'template.tex')
        templateFile = codecs.open(templatePath, encoding="utf-8", mode='r')
        self.template = templateFile.read()
        templateFile.close()

        self.email_addresses = []
        if config.has_section('report'):
            emailRe = re.compile('email([0-9]+)')
            for key, value in config.items('report'):
                ma = emailRe.fullmatch(key)
                if ma:
                    self.email_addresses.append(value)

        self.smtp_user = None
        self.smtp_pass = None
        smtp_cred = config.get('email', 'smtp_cred', fallback=None)
        if smtp_cred:
            credFile = open(smtp_cred, 'r')
            lines = credFile.readlines()
            credFile.close()
            if len(lines) != 2:
                self.logger.error('Credentials file needs two lines!')
            else:
                self.smtp_user = lines[0].strip()
                self.smtp_pass = lines[1].strip()

    def generate(self, alarm, route):

        tempDir = tempfile.mkdtemp(prefix='alarm-')
        devNull = open(os.devnull, 'w')

        targetPixmap = Map.getTargetPixmap(alarm.lat, alarm.lon,
                                           self.map_width, self.map_height,
                                           route[0], self.config, self.logger)
        targetPixmap.save(os.path.join(tempDir, 'target.png'))

        cmd = ['convert', 'target.png', 'eps3:target.eps']
        self.logger.info('Running %s', cmd)
        convert = subprocess.Popen(cmd, cwd=tempDir, stdout=devNull)
        convert.wait()

        if convert.returncode != 0:
            self.logger.error('convert failed')
            devNull.close()
            return

        routePixmap, markerRects = Map.getRoutePixmap(
            alarm.lat, alarm.lon, self.map_width, self.map_height,
            route[0], self.config, self.logger)
        routePixmap.save(os.path.join(tempDir, 'route.png'))

        cmd = ['convert', 'route.png', 'eps3:route.eps']
        self.logger.info('Running %s', cmd)
        convert = subprocess.Popen(cmd, cwd=tempDir, stdout=devNull)
        convert.wait()

        if convert.returncode != 0:
            self.logger.error('convert failed')
            devNull.close()
            return

        variables = {}

        logo = self.config.get("report", "logo", fallback=None)
        if logo:
            variables['logo'] = logo
        else:
            variables['logo'] = ''

        variables['title'] = escapeLaTeX(alarm.title())
        variables['address'] = escapeLaTeX(alarm.address())
        variables['object_name'] = escapeLaTeX(alarm.objektname)
        esc = alarm.eskalation
        if esc == '-':
            esc = ''
        variables['escalation'] = escapeLaTeX(esc)
        variables['attention'] = escapeLaTeX(alarm.attention())
        variables['location_hint'] = escapeLaTeX(alarm.ortshinweis)
        variables['contact'] = escapeLaTeX(alarm.callerInfo())
        variables['object_plan'] = escapeLaTeX(alarm.objektnummer)
        sig = 'ja'
        if alarm.sondersignal is not None and not alarm.sondersignal:
            sig = 'nein'
        variables['signal'] = escapeLaTeX(sig)
        einh = alarm.alarmiert()
        variables['resources'] = escapeLaTeX(einh)
        if alarm.datetime:
            variables['datetime'] = \
                alarm.datetime.strftime('%Y-%m-%d %H:%M:%S')
        else:
            variables['datetime'] = ''
        variables['number'] = escapeLaTeX(alarm.number)
        image = alarm.imageBase()
        if image:
            variables['image'] = image
        else:
            variables['image'] = ''

        try:
            templateOutput = Template(self.template, searchList=variables)
        except Exception:
            self.logger.exception('Failed to process template:')
            devNull.close()
            return

        texBase = 'alarm'
        texPath = os.path.join(tempDir, texBase) + '.tex'
        self.logger.info('Creating LaTeX file %s', texPath)

        outFile = codecs.open(texPath, encoding='utf-8', mode='w')
        outFile.write(str(templateOutput))
        outFile.close()

        self.logger.info('Starting LaTeX processing...')

        templateDir = self.config.get("report", "template_dir",
                                      fallback="report")
        imageDir = self.config.get("display", "image_dir",
                                   fallback="images")
        inputPaths = [templateDir, imageDir]
        inputs = '.:'
        for path in inputPaths:
            inputs += ':' + os.path.abspath(path)
        latexEnv = os.environ
        self.logger.info('TEXINPUTS=%s', inputs)
        latexEnv['TEXINPUTS'] = inputs

        cmd = ['latex', '-interaction=batchmode', texPath]
        self.logger.info('Running %s', cmd)
        latex = subprocess.Popen(cmd, cwd=tempDir, stdout=devNull,
                                 env=latexEnv)
        latex.wait()

        if latex.returncode != 0:
            logPath = os.path.join(tempDir, texBase) + '.log'
            self.logger.error('LaTeX processing failed; see log in %s',
                              logPath)
            devNull.close()
            return

        cmd = ['dvips', texBase + '.dvi']
        self.logger.info('Running %s', cmd)
        dvips = subprocess.Popen(cmd, cwd=tempDir, stdout=devNull,
                                 stderr=devNull)
        dvips.wait()

        if dvips.returncode != 0:
            self.logger.error('DVIPS processing failed.')
            devNull.close()
            return

        psPath = os.path.join(tempDir, texBase + '.ps')
        self.logger.info('PS file %s was created.', psPath)

        printOut = self.config.getboolean("report", "print", fallback=False)
        if printOut:
            self.logger.info("Printing PS file.")

            printCmd = ['lpr']
            printer = self.config.get("report", "printer", fallback="")
            if printer:
                printCmd.append('-P')
                printCmd.append(printer)
            options = self.config.get("report", "print_options", fallback="")
            if options:
                for opt in options.split():
                    printCmd.append('-o')
                    printCmd.append(opt)
            copies = self.config.getint("report", "copies", fallback=1)
            if copies != 1:
                printCmd.append('-#')
                printCmd.append(str(copies))
            printCmd.append(psPath)

            self.logger.info("Print command: %s", repr(printCmd))

            lpr = subprocess.Popen(printCmd)
            lpr.wait()
            if lpr.returncode != 0:
                self.logger.error('lpr failed.')

            self.logger.info("Print ready.")

        self.logger.info('Copying PS file...')

        targetDir = self.config.get("report", "output_dir", fallback=".")

        targetBase = alarm.dateString()
        psFileName = targetBase + '.ps'
        psFullPath = os.path.join(targetDir, psFileName)
        shutil.copy(psPath, psFullPath)
        self.logger.info('PS file copied to %s.', psFullPath)

        self.logger.info('Deleting temporary directory.')
        shutil.rmtree(tempDir)

        if self.email_addresses:
            cmd = ['ps2pdf', psFileName]
            self.logger.info('Running %s', cmd)
            ps2pdf = subprocess.Popen(cmd, cwd=targetDir, stdout=devNull)
            ps2pdf.wait()

            if ps2pdf.returncode != 0:
                self.logger.error('ps2pdf failed')
                devNull.close()
                return

            pdfFileName = targetBase + '.pdf'
            pdfFullPath = os.path.join(targetDir, pdfFileName)

            for addr in self.email_addresses:
                try:
                    self.send_mail(addr, alarm, pdfFullPath)
                except Exception:
                    self.logger.exception('Sending mail to %s failed:', addr)

        devNull.close()

    def wakeupPrinter(self):
        printOut = self.config.getboolean("report", "print", fallback=False)
        wakeupDoc = self.config.get("report", "wakeup_document",
                                    fallback="")

        if not printOut or not wakeupDoc:
            return

        self.logger.info("Waking up printer.")

        printCmd = ['lpr']
        printer = self.config.get("report", "printer", fallback="")
        if printer:
            printCmd.append('-P')
            printCmd.append(printer)
        printCmd.append(wakeupDoc)

        self.logger.info("Wakeup command: %s", repr(printCmd))

        lpr = subprocess.Popen(printCmd)
        lpr.wait()
        if lpr.returncode != 0:
            self.logger.error('Wakeup failed.')

        self.logger.info("Wakeup succeeded.")

    def send_mail(self, receiver, alarm, pdf_path):

        from_addr = self.config.get("email", "from_addr",
                                    fallback='no-reply@feuerwehr-kleve.de')
        smtp_host = self.config.get("email", "smtp_host", fallback=None)
        smtp_port = self.config.getint("email", "smtp_port", fallback=587)

        if not smtp_host or not self.smtp_user or not self.smtp_pass:
            return

        if self.logger:
            self.logger.info('Sending mail to %s.', receiver)

        subject = 'Alarmdepesche %s' % (alarm.title(), )
        content = 'Anbei die Alarmdepesche zum Einsatz %s.' % (alarm.number, )

        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg.attach(MIMEText(content, "plain"))

        with open(pdf_path, "rb") as f:
            attach = MIMEApplication(f.read(), _subtype="pdf")
        attach.add_header('Content-Disposition', 'attachment',
                          filename=os.path.basename(pdf_path))
        msg.attach(attach)

        if 'From' in msg:
            del msg['From']
        msg['From'] = from_addr

        if 'To' in msg:
            del msg['To']
        msg['To'] = receiver

        if 'Date' in msg:
            del msg['Date']
        msg['Date'] = email.utils.formatdate(localtime=True)

        s = smtplib.SMTP(smtp_host, smtp_port)
        s.starttls()
        s.login(self.smtp_user, self.smtp_pass)
        s.sendmail(from_addr, [receiver], msg.as_string())
        s.quit()

# ----------------------------------------------------------------------------
