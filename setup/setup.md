# Install

Raspbian buster

```bash
unzip -p 2021-05-07-raspios-buster-armhf-lite.zip | sudo dd status=progress of=/dev/mmcblk0 bs=4M conv=fsync
```

# Raspi-config

- System Options / Hostname: alarmxxxx
- Localisation Options / Locale: de_DE@UTF_8
- Localisation Options / Timezone: Europa/Berlin
- Localisation Options / Match Keyboard layout
- Interface Options / SSH / enable

# Kernel Configuration

Copy [this configuration file](config.txt) to /boot/config.txt

It sets the correct video mode and enables sound via HDMI.

# Network

```bash
vi /etc/dhcpcd.conf
```

```config
# Example static IP configuration:
interface eth0
static ip_address=...
static ip6_address=...
static routers=...
static domain_name_servers=...
```

# Users

```bash
passwd pi
passwd root
```

# Software

```bash
sudo apt-get update

sudo apt-get install \
    cec-utils \
    command-not-found \
    git \
    imagemagick \
    libsox-fmt-mp3 \
    pandoc \
    pulseaudio-utils \
    python-libcec \
    python3-babel \
    python3-caldav \
    python3-cheetah \
    python3-icalendar \
    python3-lxml \
    python3-mpltoolkits.basemap \
    python3-numpy \
    python3-openssl \
    python3-pip \
    python3-pyproj \
    python3-pyqt5 \
    python3-pyqt5.qtsvg \
    python3-requests \
    python3-rpi.gpio \
    python3-serial \
    python3-urllib3 \
    python3-websocket \
    rrdtool \
    scrot \
    sox \
    vim \
    x11-xserver-utils \
    x11vnc \
    xinit

# only if printing is needed
sudo apt-get install \
    cups \
    cups-bsd \
    texlive-fonts-recommended \
    texlive-latex-base \
    texlive-latex-recommended

sudo apt-get upgrade

sudo pip3 install \
    astral \
    backports.zoneinfo \
    gtts \
    imapclient \
    websocket-client
```

# Alarmdisplay Data

```bash
sudo mkdir -p /var/alarmdisplay/db
sudo mkdir -p /var/alarmdisplay/reports
sudo mkdir -p /opt/alarmdisplay/tiles
```

# Alarmdisplay Software

```bash
git clone https://gitlab.com/florianpose/alarmdisplay.git

cd alarmdisplay
sudo cp alarmdisplay-sample.conf /etc/alarmdisplay.conf
sudo vi /etc/alarmdisplay.conf

sudo cp alarmdisplay.service /etc/systemd/system
sudo systemctl enable alarmdisplay.service
sudo systemctl start alarmdisplay.service
```
