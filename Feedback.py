# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Alamos Feedback
#
# Copyright (C) 2021-2023 Florian Pose
#
# This file is part of Alarm Display.
#
# Alarm Display is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alarm Display is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alarm Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import json

from PyQt5 import QtCore
from PyQt5.QtCore import QTimer, QUrl
from PyQt5.QtNetwork import QNetworkAccessManager, QNetworkRequest, \
    QNetworkReply


# ----------------------------------------------------------------------------

class Feedback(QtCore.QObject):

    feedbackChanged = QtCore.pyqtSignal()

    url = ("https://alamos-backend.ey.r.appspot.com/"
           "fe2/feedback?dbId={dbId}&sharedSecret={secret}")

    def __init__(self, config, logger):
        super(Feedback, self).__init__()

        self.config = config
        self.logger = logger

        self.alarmMap = {}

        self.updateTimer = QTimer(self)
        self.updateTimer.setInterval(10000)
        self.updateTimer.setSingleShot(True)
        self.updateTimer.timeout.connect(self.request)

        self.networkAccessManager = QNetworkAccessManager()
        self.networkAccessManager.finished.connect(self.handleResponse)

        self.pendingRequests = []

    def alarms(self):
        return self.alarmMap.values()

    def add(self, alarm):
        if not alarm.feedbackAvailable():
            return
        url = QUrl(self.url.format(dbId=alarm.feedbackDbId,
                                   secret=alarm.feedbackSecret))
        if url in self.alarmMap:
            # URL already in map
            return
        if not self.alarmMap:
            # Alarm map was empty before. Start polling.
            self.updateTimer.start()
        self.alarmMap[url] = alarm
        self.logger.info("Added %s to feedback queries.", alarm.unit_address)

    def clear(self):
        self.logger.info("Clearing feedback queries.")
        self.alarmMap = {}
        self.pendingRequests = []
        self.updateTimer.stop()

    def request(self):
        self.logger.info("Requesting %u feedbacks.", len(self.alarmMap))
        for url, alarm in self.alarmMap.items():
            try:
                req = QNetworkRequest(url)
                self.networkAccessManager.get(req)
                self.pendingRequests.append(req)
            except Exception:
                self.logger.exception('Failed to query feedback:')

    def handleResponse(self, reply):
        req = reply.request()
        er = reply.error()
        if er == QNetworkReply.NoError:
            """
            {
                "keyword": "",
                "feedbackId": "null",
                "timestamp": 1680282255071,
                "lstOfFeedbacks": [
                    {
                        "name": "Greiffen Heinrich",
                        "id": 5028365775718912,
                        "functions": "AGT",
                        "groups": "Reichshof;A-Team",
                        "state": "RECEIVED",
                        "timeOfUpdate": 1680284283856
                    },
                    {
                        "name": "Müller Florian",
                        "id": 4842146833896328,
                        "functions": "AGT",
                        "groups": "Reichshof",
                        "state": "NO",
                        "timeOfUpdate": 1680284283855
                    },
                    {
                        "name": "Meier Raphael",
                        "id": 5397250502816840,
                        "functions": "AGT",
                        "groups": "Meinbach",
                        "state": "YES",
                        "timeOfUpdate": 1680282419434
                    }
                ]
            }
            """
            try:
                bytes_string = reply.readAll()
                doc = json.loads(bytes_string.data())
                yes = 0
                maybe = 0
                no = 0
                if 'lstOfFeedbacks' in doc:
                    for fb in doc['lstOfFeedbacks']:
                        if 'state' in fb:
                            state = fb['state']
                            if state == 'YES':
                                yes += 1
                            elif state == 'NO':
                                no += 1
                            elif state == 'READ':
                                pass
                            elif state == 'RECEIVED':
                                pass
                            else:
                                maybe += 1
                alarm = self.alarmMap[req.url()]
                prevFeedback = alarm.feedback
                alarm.feedback = (yes, maybe, no)
                if prevFeedback != alarm.feedback:
                    self.logger.info("Feedback for %s changed to %s",
                                     alarm.unit_address, alarm.feedback)
                    self.feedbackChanged.emit()
            except Exception:
                self.logger.exception('Failed to evaluate feedback:')

        else:
            self.logger.error("Feedback response error: %s", er)

        reply.deleteLater()

        if req in self.pendingRequests:
            self.pendingRequests.remove(req)
        else:
            self.logger.error("Unexpected feedback request.")

        if not self.pendingRequests:
            self.updateTimer.start()

# ----------------------------------------------------------------------------
