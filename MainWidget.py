# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Main Widget
#
# Copyright (C) 2018-2024 Florian Pose
#
# This file is part of Alarm Display.
#
# Alarm Display is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alarm Display is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alarm Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import subprocess
import datetime
import re
import time
import json
import uuid

from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QApplication, QWidget, QStackedWidget, QLabel, \
    QVBoxLayout, QAction
from PyQt5.QtCore import Qt, QTimer, QThread, QDateTime, QEventLoop, \
    pyqtSignal

from IdleWidget import IdleWidget
from AlarmWidget import AlarmWidget
from Map import getRoute
from AlarmReceiver import AlarmReceiver
from WebsocketReceiver import WebsocketReceiver
from SocketListener import SocketListener
from AlarmReport import AlarmReport
from CecCommand import CecCommand
from Alarm import Alarm, EinsatzMittel
from Forwarder import Forwarder
from Notifier import Notifier
from Sound import Sound
from GpioControl import GpioControl
from TextToSpeech import TextToSpeech
from StatusWidget import StatusWidget
from Feedback import Feedback


# ----------------------------------------------------------------------------

class MainWidget(QWidget):

    queueAlarm = pyqtSignal(Alarm)

    def __init__(self, config, logger):
        super(MainWidget, self).__init__()

        self.config = config
        self.logger = logger

        self.imageDir = self.config.get("display", "image_dir",
                                        fallback="images")

        self.queueAlarm.connect(self.processAlarm, Qt.QueuedConnection)

        self.alarm = None
        self.initialAlarmDateTime = None
        self.route = ([], None, None, None)
        self.seenPager = False
        self.seenXml = False
        self.seenJson = False
        self.reportDone = False
        self.forwarder = Forwarder(config, logger)
        self.notifier = Notifier(config, logger)
        self.sound = Sound(config, logger)
        self.gpioControl = GpioControl(config, logger)
        self.tts = TextToSpeech(config, logger)
        self.feedback = Feedback(config, logger)
        self.feedback.feedbackChanged.connect(self.feedbackChanged)

        self.reportTimer = QTimer(self)
        self.reportTimer.setInterval(
            self.config.getint("report", "timeout", fallback=60) * 1000)
        self.reportTimer.setSingleShot(True)
        self.reportTimer.timeout.connect(self.reportTimeout)

        self.simTimer = QTimer(self)
        self.simTimer.setInterval(10000)
        self.simTimer.setSingleShot(True)
        self.simTimer.timeout.connect(self.simTimeout)
        # self.simTimer.start()

        self.idleTimer = QTimer(self)
        idleTimeout = self.config.getint("display", "idle_timeout",
                                         fallback=30)
        self.idleTimer.setInterval(idleTimeout * 60000)
        self.idleTimer.setSingleShot(True)
        self.idleTimer.timeout.connect(self.idleTimeout)

        self.screenTimer = QTimer(self)
        screenTimeout = self.config.getint("display", "screen_timeout",
                                           fallback=0)
        self.screenTimer.setInterval(screenTimeout * 60000)
        self.screenTimer.setSingleShot(True)
        self.screenTimer.timeout.connect(self.screenTimeout)
        if self.screenTimer.interval() > 0:
            self.screenTimer.start()

        # Presence -----------------------------------------------------------

        self.presenceTimer = QTimer(self)
        self.presenceTimer.setInterval(1000)
        self.presenceTimer.setSingleShot(False)
        self.presenceTimer.timeout.connect(self.checkPresence)
        self.presenceTimer.start()

        self.switchOnTimes = []
        self.switchOffTimes = []

        if self.config.has_section('presence'):
            onRe = re.compile('on[0-9]+')
            offRe = re.compile('off[0-9]+')

            for key, value in self.config.items('presence'):
                ma = onRe.fullmatch(key)
                if ma:
                    tup = self.parsePresence(key, value)
                    if tup:
                        self.switchOnTimes.append(tup)
                    continue
                ma = offRe.fullmatch(key)
                if ma:
                    tup = self.parsePresence(key, value)
                    if tup:
                        self.switchOffTimes.append(tup)
                    continue

        self.updateNextSwitchTimes()

        # Appearance ---------------------------------------------------------

        self.logger.info('Setting up X server...')

        subprocess.call(['xset', 's', 'off'])
        subprocess.call(['xset', 's', 'noblank'])
        subprocess.call(['xset', 's', '0', '0'])
        subprocess.call(['xset', '-dpms'])

        self.move(0, 0)
        self.resize(1920, 1080)

        self.setWindowTitle('Alarmdisplay')

        self.setStyleSheet("""
            font-size: 60px;
            background-color: rgb(0, 34, 44);
            color: rgb(2, 203, 255);
            font-family: "DejaVu Sans";
            """)

        # Sub-widgets --------------------------------------------------------

        layout = QVBoxLayout(self)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        self.stackedWidget = QStackedWidget(self)
        layout.addWidget(self.stackedWidget)

        self.idleWidget = IdleWidget(self)
        self.idleWidget.start()
        self.stackedWidget.addWidget(self.idleWidget)

        self.alarmWidget = AlarmWidget(self)
        self.stackedWidget.addWidget(self.alarmWidget)

        self.errorWidget = QLabel(self)
        self.errorWidget.setGeometry(self.contentsRect())
        self.errorWidget.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        self.errorWidget.setStyleSheet("""
            background-color: transparent;
            font-size: 20px;
            color: red;
            """)

        # Shortcuts ----------------------------------------------------------

        action = QAction(self)
        action.setShortcut(QKeySequence("1"))
        action.setShortcutContext(Qt.ApplicationShortcut)
        action.triggered.connect(self.exampleJugend)
        self.addAction(action)

        action = QAction(self)
        action.setShortcut(QKeySequence("2"))
        action.setShortcutContext(Qt.ApplicationShortcut)
        action.triggered.connect(self.exampleEngels)
        self.addAction(action)

        action = QAction(self)
        action.setShortcut(QKeySequence("3"))
        action.setShortcutContext(Qt.ApplicationShortcut)
        action.triggered.connect(self.exampleSack)
        self.addAction(action)

        action = QAction(self)
        action.setShortcut(QKeySequence("4"))
        action.setShortcutContext(Qt.ApplicationShortcut)
        action.triggered.connect(self.exampleWolfsgrabenPager)
        self.addAction(action)

        action = QAction(self)
        action.setShortcut(QKeySequence("5"))
        action.setShortcutContext(Qt.ApplicationShortcut)
        action.triggered.connect(self.exampleWolfsgrabenMail)
        self.addAction(action)

        action = QAction(self)
        action.setShortcut(QKeySequence("6"))
        action.setShortcutContext(Qt.ApplicationShortcut)
        action.triggered.connect(self.exampleWald)
        self.addAction(action)

        action = QAction(self)
        action.setShortcut(QKeySequence("7"))
        action.setShortcutContext(Qt.ApplicationShortcut)
        action.triggered.connect(self.exampleStadtwerkePager)
        self.addAction(action)

        action = QAction(self)
        action.setShortcut(QKeySequence("8"))
        action.setShortcutContext(Qt.ApplicationShortcut)
        action.triggered.connect(self.exampleLebenshilfe)
        self.addAction(action)

        action = QAction(self)
        action.setShortcut(QKeySequence("9"))
        action.setShortcutContext(Qt.ApplicationShortcut)
        action.triggered.connect(self.exampleKrankenhausOhne)
        self.addAction(action)

        action = QAction(self)
        action.setShortcut(QKeySequence("0"))
        action.setShortcutContext(Qt.ApplicationShortcut)
        action.triggered.connect(self.exampleKrankenhaus)
        self.addAction(action)

        # Threads ------------------------------------------------------------

        self.receiverThread = QThread()
        self.alarmReceiver = AlarmReceiver(self.config, self.logger)
        self.alarmReceiver.receivedAlarm.connect(self.receivedPagerAlarm)
        self.alarmReceiver.finished.connect(self.receiverThread.quit)
        self.alarmReceiver.errorMessage.connect(self.receiverError)
        self.alarmReceiver.moveToThread(self.receiverThread)
        self.receiverThread.started.connect(self.alarmReceiver.receive)
        self.receiverThread.start()

        self.websocketReceiverThread = QThread()
        self.websocketReceiver = WebsocketReceiver(self.config, self.logger)
        self.websocketReceiver.receivedAlarm.connect(
            self.receivedWebsocketAlarm)
        self.websocketReceiver.finished.connect(
            self.websocketReceiverThread.quit)
        self.websocketReceiver.moveToThread(self.websocketReceiverThread)
        self.websocketReceiverThread.started.connect(
            self.websocketReceiver.receive)
        self.websocketReceiverThread.start()

        if self.websocketReceiver.status:
            self.statusWidget = StatusWidget(self)
            layout.addWidget(self.statusWidget)
            self.websocketReceiver.receivedStatus.connect(
                self.statusWidget.setStatus)

        if self.config.has_section('email') and \
                self.config.get("email", "imap_host", fallback=''):
            from ImapMonitor import ImapMonitor
            self.imapThread = QThread()
            self.imapMonitor = ImapMonitor(self.config, self.logger)
            self.imapMonitor.receivedAlarm.connect(self.receivedXmlAlarm)
            self.imapMonitor.moveToThread(self.imapThread)
            self.imapMonitor.finished.connect(self.imapThread.quit)
            self.imapThread.started.connect(self.imapMonitor.start)
            self.imapThread.start()

        self.socketListener = SocketListener(self.logger)
        self.socketListener.pagerAlarm.connect(self.receivedPagerAlarm)
        self.socketListener.xmlAlarm.connect(self.receivedXmlAlarm)

        self.cecThread = QThread()
        self.cecThread.start()
        self.cecCommand = CecCommand(self.logger)
        self.cecCommand.moveToThread(self.cecThread)

        self.report = AlarmReport(self.config, self.logger)

        try:
            self.notifier.startup()
        except Exception:
            self.logger.exception('Startup notification failed:')

        self.logger.info('Setup finished.')

    # ------------------------------------------------------------------------

    def parsePresence(self, key, value):
        dateRe = re.compile(r'(\S+)\s(\d+):(\d+)')
        ma = dateRe.fullmatch(value)
        if not ma:
            self.logger.error("Invalid date spec for %s: %s", key, value)
            return

        hour = int(ma.group(2))
        if hour >= 24:
            self.logger.error("Invalid hour in %s: %s", key, hour)
            return

        minute = int(ma.group(3))
        if minute >= 60:
            self.logger.error("Invalid minute in %s: %s", key, minute)
            return

        try:
            tm = time.strptime(ma.group(1), '%a')
        except Exception:
            self.logger.exception("Invalid week day in %s: %s",
                                  key, ma.group(1))
            return

        weekDay = tm.tm_wday
        return (weekDay, hour, minute)

    def updateNextSwitchTimes(self):
        self.nextSwitchOn = self.findNextEvent(self.switchOnTimes)
        self.logger.info("Next switch on: %s", self.nextSwitchOn)
        self.nextSwitchOff = self.findNextEvent(self.switchOffTimes)
        self.logger.info("Next switch off: %s", self.nextSwitchOff)

    def findNextEvent(self, tups):
        nextDt = None
        now = datetime.datetime.now()
        for tup in tups:
            date = now.date()
            dayDiff = (tup[0] - date.weekday() + 7) % 7
            date = date + datetime.timedelta(days=dayDiff)
            dt = datetime.datetime(date.year, date.month, date.day,
                                   tup[1], tup[2])
            if dt < now:
                dt = dt + datetime.timedelta(days=7)
            if not nextDt or nextDt > dt:
                nextDt = dt
        return nextDt

    def checkPresence(self):
        now = datetime.datetime.now()
        update = False
        if self.nextSwitchOn and now >= self.nextSwitchOn:
            update = True
            self.cecCommand.switchOn()
        elif self.nextSwitchOff and now >= self.nextSwitchOff:
            update = True
            # only switch off, if no alarm active
            alarm = self.idleTimer.isActive() or self.screenTimer.isActive()
            if not alarm:
                self.cecCommand.switchOff()
        if update:
            self.updateNextSwitchTimes()

    # ------------------------------------------------------------------------

    def receivedPagerAlarm(self, pagerStr):
        self.logger.info('Received pager alarm: %s', repr(pagerStr))

        alarm = Alarm(self.config)
        alarm.fromPager(pagerStr, self.logger)

        try:
            self.notifier.pager(pagerStr)
        except Exception:
            self.logger.exception('Pager notification failed:')

        self.queueAlarm.emit(alarm)

    # ------------------------------------------------------------------------

    def receiverError(self, errorMessage):
        self.errorWidget.setText(errorMessage)

    # ------------------------------------------------------------------------

    def receivedWebsocketAlarm(self, data):
        self.logger.info('Received websocket alarm: %s', repr(data))

        alarm = Alarm(self.config)

        try:
            alarm.fromAlamos(data, self.logger)
        except Exception:
            self.logger.exception('Failed to process websocket alarm:')
            return

        self.queueAlarm.emit(alarm)

    # ------------------------------------------------------------------------

    def receivedXmlAlarm(self, xmlContent):
        self.logger.info('Received XML alarm.')

        alarm = Alarm(self.config)

        try:
            alarm.fromXml(xmlContent, self.logger)
        except Exception:
            self.logger.exception('Failed to parse XML:')
            return

        self.queueAlarm.emit(alarm)

    # ------------------------------------------------------------------------

    def processAlarm(self, newAlarm):

        now = QDateTime.currentDateTime()
        processUuid = uuid.uuid4()

        self.logger.info(f"Processing alarm: {processUuid}")

        self.alarmWidget.setHourGlass(True)
        self.processEvents(processUuid)

        try:
            newAlarm.save()
        except Exception:
            self.logger.exception('Failed to save alarm:')

        try:
            self.forwarder.forward(newAlarm)
        except Exception:
            self.logger.exception('Failed to forward alarm:')

        if not self.alarm or not self.alarm.matches(newAlarm):

            # If an incomplete pager alarm is received right after a valid
            # one, ignore the incomplete alarm
            if newAlarm.fallbackStr \
                    and self.alarm \
                    and not self.alarm.fallbackStr \
                    and self.initialAlarmDateTime.secsTo(now) < 90:
                self.logger.warning("Ignoring fallback alarm.")
                self.alarmWidget.setHourGlass(False)
                self.logger.info(f"Finished processing alarm: {processUuid}")
                return

            # process recent report first
            if self.alarm and not self.reportDone:
                self.logger.info(("Processing unfinished report "
                                  "before displaying new alarm."))
                self.reportTimer.stop()
                self.generateReport(processUuid)

            self.logger.info("Processing new alarm.")

            # Display alarm widget
            self.idleWidget.stop()
            self.alarmWidget.startTimer(now)
            self.stackedWidget.setCurrentWidget(self.alarmWidget)

            # Switch screen on
            self.cecCommand.switchOn()

            # Clear feedback
            self.feedback.clear()
            self.alarmWidget.updateFeedback()

            # Set current alarm
            self.alarm = newAlarm

            # Reset flags
            self.route = ([], None, None, None)
            self.seenPager = False
            self.seenXml = False
            self.seenJson = False
            self.reportDone = False
            self.initialAlarmDateTime = now

            # Start timers and services for new alarm
            self.reportTimer.start()
            self.sound.start()
            self.tts.clear()
            self.tts.start()
            self.gpioControl.trigger()
            self.report.wakeupPrinter()
        else:
            # Merge existing alarm
            self.alarm.merge(newAlarm, self.logger)

        # Merge sources
        if newAlarm.source == 'pager':
            self.seenPager = True
        elif newAlarm.source == 'xml':
            self.seenXml = True
        elif newAlarm.source == 'json':
            self.seenJson = True
        self.logger.info('Sources: %s', self.alarm.sources)

        # If possible, add alarm to feedback watcher
        self.feedback.add(newAlarm)

        # Update alarm widget information
        self.alarmWidget.processAlarm(self.alarm)
        self.feedbackChanged()  # prepare feedback display

        # Reset idle and screen timers
        if self.idleTimer.interval() > 0:
            self.idleTimer.start()
        if self.screenTimer.interval() > 0:
            self.screenTimer.start()

        self.processEvents(processUuid)

        self.logger.info('Route query...')
        self.route = getRoute(self.alarm, self.config, self.logger,
                              self.route)
        self.alarmWidget.setRoute(self.route)
        self.logger.info('Route ready.')

        # Update TTS message
        self.tts.setText(self.alarm.spoken())

        # Information for report complete: generate report immediately
        #
        # Since 2023-03 the JSON dataset contains all necessary information.
        # Combined pager and XML datasets are also sufficient.
        #
        if (self.seenJson or (self.seenPager and self.seenXml)) \
                and not self.reportDone:
            self.reportTimer.stop()
            self.logger.info('Report information complete.')
            self.generateReport(processUuid)

        self.alarmWidget.setHourGlass(False)
        self.logger.info(f"Finished processing alarm: {processUuid}")

    def processEvents(self, identifier=None):
        self.logger.info(f"Processing events: {identifier}")
        ret = None
        try:
            ret = QApplication.processEvents(
                QEventLoop.ExcludeUserInputEvents
                | QEventLoop.ExcludeSocketNotifiers)
        except Exception:
            self.logger.exception(f"Error processing events: {identifier}")
        self.logger.info(f"Back from processing events: {identifier} {ret}")

    def reportTimeout(self):
        self.logger.info('Report timeout reached.')
        self.generateReport("report-timeout")

    def generateReport(self, identifier=None):
        if self.reportDone:
            self.logger.info('Report already done.')
            return

        self.reportDone = True
        self.alarmWidget.setHourGlass(True)
        self.processEvents(f"{identifier}-1")

        self.logger.info('Report...')
        try:
            self.report.generate(self.alarm, self.route)
        except Exception:
            self.logger.exception('Report failed:')
            self.reportDone = False

        self.logger.info('Report finished.')
        self.alarmWidget.setHourGlass(False)
        self.processEvents(f"{identifier}-2")

    def simTimeout(self):
        self.exampleJugend()

    def idleTimeout(self):
        self.logger.info('Switching to idle mode.')
        self.stackedWidget.setCurrentWidget(self.idleWidget)
        self.idleWidget.start()
        self.feedback.clear()
        self.alarmWidget.updateFeedback()

    def screenTimeout(self):
        self.cecCommand.switchOff()

    def feedbackChanged(self):
        try:
            self.alarmWidget.updateFeedback(self.feedback.alarms())
        except Exception:
            self.logger.exception('Update feedback failed:')

    # ------------------------------------------------------------------------

    def exampleJugend(self):
        alarm = Alarm(self.config)
        alarm.source = 'xml'
        alarm.sources.add(alarm.source)
        alarm.number = '40001'
        alarm.datetime = datetime.datetime.now().astimezone()
        alarm.art = 'B'
        alarm.stichwort = '3'
        alarm.diagnose = 'Wohnungsbrand'
        alarm.strasse = 'St.-Anna-Berg'
        alarm.ort = 'Kleve'
        alarm.hausnummer = '5'
        alarm.objektname = 'Jugendherberge'
        alarm.besonderheit = 'lt. Betreiber 34 Personen gemeldet'
        alarm.objektnummer = 'KLV 02/140'
        alarm.lat = 51.78317
        alarm.lon = 6.10695

        self.queueAlarm.emit(alarm)

    def exampleEngels(self):
        alarm = Alarm(self.config)
        alarm.source = 'xml'
        alarm.sources.add(alarm.source)
        alarm.number = '40002'
        alarm.datetime = datetime.datetime.now().astimezone()
        alarm.art = 'H'
        alarm.stichwort = '1'
        alarm.diagnose = 'Tierrettung'
        alarm.ort = 'Kleve'
        alarm.ortsteil = 'Reichswalde'
        alarm.strasse = 'Engelsstraße'
        alarm.hausnummer = '5'
        alarm.ort = 'Kleve'
        alarm.besonderheit = 'Katze auf Baum'
        alarm.sondersignal = False
        alarm.lat = 51.75065
        alarm.lon = 6.11170

        self.queueAlarm.emit(alarm)

    def exampleSack(self):
        self.logger.info('Example Sackstrasse')

        alarm = Alarm(self.config)
        alarm.source = 'xml'
        alarm.sources.add(alarm.source)
        alarm.number = '40003'
        alarm.datetime = datetime.datetime.now().astimezone()
        alarm.art = 'B'
        alarm.stichwort = '2'
        alarm.diagnose = 'Garagenbrand'
        alarm.strasse = 'Sackstraße'
        alarm.hausnummer = '173'
        alarm.ort = 'Kleve'
        alarm.besonderheit = 'Kfz brennt unter Carport'
        alarm.lat = 51.77190
        alarm.lon = 6.12305

        self.queueAlarm.emit(alarm)

    def exampleWolfsgrabenPager(self):

        pagerStr = '21-12-17 11:55:10 LG Reichswalde Geb{udesteuerung' + \
            ' #K01;N5175638E0611815; *40004*B2 Kaminbrand**Kleve*' + \
            'Reichswalde*Wolfsgraben*11**'

        alarm = Alarm(self.config)
        alarm.fromPager(pagerStr, self.logger)

        self.queueAlarm.emit(alarm)

    def exampleWolfsgrabenMail(self):

        alarm = Alarm(self.config)
        alarm.source = 'xml'
        alarm.sources.add(alarm.source)
        alarm.datetime = datetime.datetime.now().astimezone()
        alarm.art = 'B'
        alarm.stichwort = '2'
        alarm.diagnose = 'Kaminbrand'
        alarm.besonderheit = 'keine Personen mehr im Gebäude'
        alarm.ortsteil = 'Reichswalde'
        alarm.strasse = 'Wolfsgraben'
        alarm.hausnummer = '11'
        alarm.ort = 'Kleve'
        alarm.lat = 51.75638
        alarm.lon = 6.11815
        alarm.meldender = 'Müller'
        alarm.rufnummer = '0179 555 364532'
        alarm.number = '1170040004'
        alarm.sondersignal = True
        em = EinsatzMittel('FW', 'KLV', '05', 'LF10', '1', '')
        alarm.einsatzmittel.add(em)
        em = EinsatzMittel('FW', 'KLV', '02', 'LF20', '1', '')
        alarm.einsatzmittel.add(em)

        self.queueAlarm.emit(alarm)

    def exampleWald(self):
        pagerStr = '16-12-17 18:55:10 LG Reichswalde Gebäudesteuerung' + \
            ' #K01;N5173170E0606900; *40005*H1 Hilfeleistung*' + \
            'Eichhörnchen auf Baum*Kleve*Reichswalde*' + \
            'Grunewaldstrasse***Waldweg C'

        alarm = Alarm(self.config)
        alarm.fromPager(pagerStr, self.logger)

        self.queueAlarm.emit(alarm)

    def exampleStadtwerkePager(self):

        pagerStr = '21-12-17 11:55:10 LG Reichswalde Gebäudesteuerung' + \
            ' #K01;N5179473E0613985; *40006*B3 Brand Bürogebäude*' + \
            'Stadtwerke Kleve GmbH*Kleve*Kleve*Flutstraße*36**'

        alarm = Alarm(self.config)
        alarm.fromPager(pagerStr, self.logger)

        self.queueAlarm.emit(alarm)

    def exampleLebenshilfe(self):

        alarm = Alarm(self.config)
        with open('test_data/2023-04-04-01-09-22.json', 'r') as f:
            data = json.loads(f.read())
            alarm.fromAlamos(data, self.logger)
        self.queueAlarm.emit(alarm)

        data["dbId"] = "6291621930991617"
        data["dbId_shared_secret"] = "it5q4v9s5iv1"
        data["address"] = "FW KLV Leiter"
        alarm2 = Alarm(self.config)
        alarm2.fromAlamos(data, self.logger)
        alarm2.feedback = (1, 0, 0)
        self.queueAlarm.emit(alarm2)

    def exampleKrankenhausOhne(self):

        pagerStr = ('02-11-22 22:38:43 LG Reichswalde Gebäudesteuerung'
                    '#K01;N5177356E0613636;*'
                    '72244*'
                    'B3 Brandmeldeanlage 3 **'
                    'Kleve*'
                    'Kleve*'
                    'Albersallee*'
                    '5*'
                    '*'
                    'Nassauer Allee - Triftstrasse')

        alarm = Alarm(self.config)
        alarm.fromPager(pagerStr, self.logger)

        self.queueAlarm.emit(alarm)

    def exampleKrankenhaus(self):

        pagerStr = ('02-11-22 22:38:43 LG Reichswalde Gebäudesteuerung'
                    '#K01;N5177356E0613636;*'
                    '72244*'
                    'B3 Brandmeldeanlage 3 **'
                    'Kleve*'
                    'Kleve*'
                    'Albersallee*'
                    '5*'
                    'KLV 01/102*'
                    'Nassauer Allee - Triftstrasse')

        alarm = Alarm(self.config)
        alarm.fromPager(pagerStr, self.logger)

        self.queueAlarm.emit(alarm)

# ----------------------------------------------------------------------------
